import cv2
import re
import pytesseract
from pipeline.pipeline import Pipeline
import pipeline.utils.fs as fs


class CaptureImages(Pipeline):
    """Pipeline task to capture images from directory."""

    def __init__(self, src, valid_exts=(".jpg", ".png"), level=None, contains=None):
        self.src = src
        self.valid_exts = valid_exts
        self.level = level
        self.contains = contains

        super().__init__()
    
    def rotate(self,image, center = None, scale = 1.0):
        angle=360-int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
    #         print(image)
        print(angle)
        (h, w) = image.shape[:2]

        if center is None:
            center = (w / 2, h / 2)

        # Perform the rotation
        if angle != 180:
            M = cv2.getRotationMatrix2D(center, angle, scale)
            rotated = cv2.warpAffine(image, M, (w, h))
        else:
            M = cv2.getRotationMatrix2D(center, 360, scale)
            rotated = cv2.warpAffine(image, M, (w, h))
        return rotated
    
    
    def generator(self):
        """Yields the image content and metadata."""

        source = fs.list_files(self.src, self.valid_exts, self.level, self.contains)
        while self.has_next():
            try:
                image_file = next(source)
                image = cv2.imread(image_file)
                rotated = self.rotate(image)
                
                data = {
                    "image_id": image_file,
                    "image": rotated
                }

                if self.filter(data):
                    yield self.map(data)
            except StopIteration:
                return

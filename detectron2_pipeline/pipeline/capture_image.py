import cv2

from pipeline.pipeline import Pipeline


class CaptureImage(Pipeline):
    """Pipeline task to capture single image file"""

    def __init__(self, src):
        self.src = src

        super().__init__()
    
    def rotate(self,image, center = None, scale = 1.0):
        angle=360-int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
    #         print(image)
        print(angle)
        (h, w) = image.shape[:2]

        if center is None:
            center = (w / 2, h / 2)

        # Perform the rotation
        if angle != 180:
            M = cv2.getRotationMatrix2D(center, angle, scale)
            rotated = cv2.warpAffine(image, M, (w, h))
        else:
            M = cv2.getRotationMatrix2D(center, 360, scale)
            rotated = cv2.warpAffine(image, M, (w, h))
        return rotated
    
    def generator(self):
        """Yields the image content and metadata."""

        image = cv2.imread(self.src)
        rotated = self.rotate(image)
        
        data = {
            "image_id": self.src,
            "image": rotated
        }

        if self.filter(data):
            yield self.map(data)
